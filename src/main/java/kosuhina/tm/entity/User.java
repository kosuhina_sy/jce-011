package kosuhina.tm.entity;


import kosuhina.tm.enumerated.Role;

public class User {

    private Long id = System.nanoTime();

    private String login;

    private String passwordHash;

    private String firstName = "";

    private String lastName = "";

    private String middleName = "";

    private Role role = Role.USER;


    public Role getRole() {
        return role;
    }

    public void setRole(Role role) { this.role = role; }

    public String getFirstName() { return firstName; }

    public Long getId() { return id; }

    public String getLogin() { return login; }

    public String getPasswordHash() { return passwordHash; }

    public String getLastName() { return lastName; }

    public String getMiddleName() { return middleName; }

    public void setId(Long id) { this.id = id; }

    public void setLogin(String login) { this.login = login; }

    public void setPasswordHash(String passwordHash) { this.passwordHash = passwordHash; }

    public void setFirstName(String firstName) { this.firstName = firstName; }

    public void setLastName(String lastName) { this.lastName = lastName; }

    public void setMiddleName(String middleName) { this.middleName = middleName; }

}

